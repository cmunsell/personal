-- MySQL dump 10.13  Distrib 5.7.13, for linux-glibc2.5 (x86_64)
--
-- Host: ryvdxs57afyjk41z.cbetxkdyhwsb.us-east-1.rds.amazonaws.com    Database: p0t4guhh5aafz1s3
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `member` int(11) DEFAULT NULL,
  `gym` int(11) DEFAULT NULL,
  `checkinTime` datetime DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES (544,1016,'2017-03-23 04:16:41',70,'2017-03-23 04:16:41','2017-03-23 04:16:41'),(559,1014,'2017-03-23 04:19:02',71,'2017-03-23 04:19:02','2017-03-23 04:19:02'),(565,1014,'2017-03-23 04:19:04',72,'2017-03-23 04:19:04','2017-03-23 04:19:04');
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gym`
--

DROP TABLE IF EXISTS `gym`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gym` (
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_swedish_ci,
  `websiteUrl` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `affiliateId` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `id` int(10) unsigned NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `affiliateId` (`affiliateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gym`
--

LOCK TABLES `gym` WRITE;
/*!40000 ALTER TABLE `gym` DISABLE KEYS */;
INSERT INTO `gym` VALUES ('Smash Gyms Sunnyvale','',NULL,'1014',1014,NULL,NULL),('Smash Gyms San Jose','',NULL,'1016',1016,NULL,NULL),('Smash Gyms Milpitas','',NULL,'1018',1018,NULL,NULL),('True Fight Club','',NULL,'1034',1034,NULL,NULL),('KB Fitness Exercise Studio','',NULL,'1038',1038,NULL,NULL),('Dark Horse Gym','',NULL,'1040',1040,NULL,NULL),('Round 5 Fitness','',NULL,'1044',1044,NULL,NULL),('Alameda Jiu-Jitsu','',NULL,'1046',1046,NULL,NULL),('Antdawgs MMA Training Center','',NULL,'1048',1048,NULL,NULL),('Mindset Martial Arts & Fitness','',NULL,'1050',1050,NULL,NULL),('Team Silva BJJ','',NULL,'1052',1052,NULL,NULL),('CoreFitness by Jeanelle','',NULL,'1054',1054,NULL,NULL),('American Kickboxing Academy Sunnyvale','',NULL,'1056',1056,NULL,NULL),('World Team USA Gym','',NULL,'1058',1058,NULL,NULL),('Modern Taekwondo','',NULL,'1060',1060,NULL,NULL),('Anchored Strength & Conditioning','',NULL,'1062',1062,NULL,NULL),('Hwa Rang Kwan Martial Arts center','',NULL,'1064',1064,NULL,NULL),('Halo Jiu Jitsu','',NULL,'1066',1066,NULL,NULL),('Claycomb Academy of Martial Arts','',NULL,'1068',1068,NULL,NULL),('Kaizen Dojo','',NULL,'1074',1074,NULL,NULL),('Pro-Faction Martial Arts & Fitness','',NULL,'1078',1078,NULL,NULL),('Knoxx Martial Arts & Fitness','',NULL,'1080',1080,NULL,NULL),('Union City Fitness & Performance ','Union City Fitness & Performance started from the idea “we are who our members are and what they are trying to become – stronger fathers, healthier mothers, confident teenagers and healthier, happier, human beings”. We wanted to create a fitness program that is all inclusive at the core, and yet, still a little different. Our doors are wide open for you.',NULL,'1082',1082,NULL,NULL),('Combat Fitness','',NULL,'1084',1084,NULL,NULL),('True 10 Fitness','',NULL,'1086',1086,NULL,NULL),('Tomacelli Academy','',NULL,'1088',1088,NULL,NULL),('Bushido Fight Team','',NULL,'1090',1090,NULL,NULL),('Contra Costa Crossfit','',NULL,'1092',1092,NULL,NULL),('Raul Castillo Martial Arts','',NULL,'1094',1094,NULL,NULL);
/*!40000 ALTER TABLE `gym` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `email` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `firstName` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `photoUrl` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `membershipType` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `homeGym` int(11) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `password` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `firstName` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `photoUrl` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `socialProfiles` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `accessLevel` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `gym` int(11) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=2006 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('$2a$10$JwuLtsE8/8af4X.vswi.buZwlDknZZdKWwrZCC3F7qVV.f1aHTsgi','john@socialfitnessnetwork.com','John','Bernardo','','{}','active',NULL,'superAdmin',NULL,1,'2017-02-02 08:33:18','2017-02-02 08:42:33'),('$2a$10$A/XyQJ3gKFLsZb7Ecp/rUedOl9xdPgdJq5XLa35Kwd5VF8dvnqkiS','erwin@socialfitnessnetwork.com','Erwin','','','{}','active',NULL,'superAdmin',NULL,2,'2017-02-02 08:33:18','2017-02-02 08:33:18'),('$2a$10$I9ClFL68VCBcdmfAP4U1fOzgvr0uYUFTLmegcuONLll9B54rIvPLi','cainan@socialfitnessnetwork.com','Cainan','','','{}','active',NULL,'superAdmin',NULL,3,'2017-02-02 08:33:18','2017-02-02 08:33:18'),('$2a$10$VEbJYKUHeGBCIGslB.oR7ueERwCZ68ZvBfjvoD.5qVNheAPUsEw1G','rudy@socialfitnessnetwork.com','Rudy','','','{}','active',NULL,'admin',NULL,4,'2017-02-02 08:45:13','2017-02-02 08:45:13'),('$2a$10$SIgKuSPV5Bl4jDqUIisUweKHaX.J4G7UugfB0W8xvhHpp/ag6YD16','iani@socialfitnessnetwork.com','Iani','','','{}','active',NULL,'admin',NULL,5,'2017-02-02 17:56:40','2017-02-02 17:56:40'),('$2a$10$pmrrVzJjl2ccTD7KipnXceq594pNx4fUQPRIqcUlNU7J1x27wprea','eli@socialfitnessnetwork.com','Eli','','','{}','active',NULL,'admin',NULL,6,'2017-02-02 17:56:49','2017-02-02 17:56:49'),('$2y$08$zzILS4Y0o/ire1aZoDdHMem366mOBInF6SWtIYQffhEix/wfeqdlO','kbfitness5@gmail.com','Kathy','Burgett',NULL,NULL,'active','144-264-4144','owner',1038,30,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$c69sjySpiGNTy/KoG9J/e.dhMKG4SaioWZwOfGM7bClNws7WDFl.u','valle.angelia@gmail.com','Angie','Valle',NULL,NULL,'active','922-420-5922','manager',1038,31,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$qG0hwQvqkCUbz9gCUdrhxe0IgeXNRbKbn7.qhu9K291TKwY6hpFyO','evanresguerra@yahoo.com','Evannuel','Esguerra',NULL,NULL,'active','841-740-7841','owner',1034,33,'2017-02-02 16:47:12','2017-04-28 22:14:01'),('$2y$08$roPOiGTf3cW62RoyQobUn.mDBJAIhRj5USjpjDnsSCpgYSf7TRKVm','parabellumtc@gmail.com','Gabe','Scarpelli',NULL,NULL,'active','090-320-5090','owner',1040,34,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$/v5qfXX75X1UfQ2OKC0nLuwvrCBCrcgP/MuOYwUhDNMCRqlfdBbZy','darkhorsegym@gmail.com','Samantha','Velasquez',NULL,NULL,'active','273-992-1273','employee',1040,35,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$ZFGS3arWlsHwHPtzfHNske4sby71wSQb2YF1BksUhuT96GeKF7fEq','jake@smashmilpitas.com','Jake','Tanenbaum',NULL,NULL,'active','600-262-5600','owner',1018,36,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$KFkzdm6io/sN0d/E9h5/wun9ZbnGTKDD8BnENRCum1cl.ty6.arYa','rudi@smashmilpitas.com','Rudi','Ott',NULL,NULL,'active','600-262-5600','owner',1018,37,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$4GBcoU83jGjXBVj2R6MqMe8Mm3HUzGEiLg.4v7zxj/6A.cWlnR/ni','leo@smashgyms.com','Leo','Shen',NULL,NULL,'active','772-246-1772','manager',1014,38,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$8717c43BJIOKFYCO..8waukx7M7RJYBYwVBkUuAR38SOrqHCZ980G','debbie@smashgyms.com','Debbie','Sanchez',NULL,NULL,'active','334-744-6334','owner',1014,39,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$w9PCwr9abcvzuJVNy27/4OsYJbbDeuWekkLpCeN1SfgyCo9jAxvKi','contact@smashgyms.com','Master','Login',NULL,NULL,'active','334-744-6334','employee',1014,40,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$V/aU0Daz.DcfmHieBOjgAetdZfXMgLWU6try1tteJZa4vjq5PWQ9C','filip@smashgyms.com','Filip','Novachkov',NULL,NULL,'active','334-472-9334','owner',1016,41,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$s9oWn9HOE0KQzCEEkMDgiehSp0Ss4e49bTBxiHv8pBIdfjanfrH4a','eli@smashgyms.com','Eli','Sanchez',NULL,NULL,'active',NULL,'owner',1018,42,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$TwydXe7HQkviQj33pxTPKufS6f3Im4Mkk9Qdz4zRwhKGwvsgA9Egm','mario@smashgyms.com','Mario','Rios',NULL,NULL,'active','624-928-1624','owner',1016,43,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$aOAhGWzgowQ7HfbVzA4TRO8czRtAhkzPvYJoiY5qWHf.aw88rfKU6','ianipenev@gmail.com','Iani','Penev',NULL,NULL,'active',NULL,'admin',NULL,45,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$3sKTtuCIQhACBCQPN4dwK..R53NP.Ph92ELFDJ29yCKm5SjluBnpi','sergiobjj@hotmail.com','Sergio','Silva',NULL,NULL,'active','700-263-8700','owner',1046,46,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$MSrZ7sbGrqKxo6MmAsY9q.Gs5RXBQSYAbrn/471SzZVQnAz9dzSK2','cafewomyn@yahoo.com','Rebecca','Trissell',NULL,NULL,'active','575-305-9575','manager',1046,47,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$5tXOddLQBSn31nExp7gKA.DBFKwpe2zSSmO3Ck.iUVtxzo4RwzyJG','dennisfdeguzman@yahoo.com','Dennis','DeGuzman',NULL,NULL,'active','137-610-6137','owner',1014,48,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$DBEu7eztA7MT43TaXuafN.PRWKPiY7pXT12FB.fwDWfrq8pGoE2hW','mikaylajune16@gmail.com','Mikayla','Calilung',NULL,NULL,'active','137-610-6137','employee',1014,49,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$mQieYGwXSkrFp4wC5hQnlulY5VOTSNgnQl/R.xSzYjmlYPCM7XTJC','spanopio@slsja.org','Samantha ','Panopio',NULL,NULL,'active','137-610-6137','employee',1044,50,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$aCBCmdqfa8QoxL4GYt5xCOCux4E.bKSyttReyWcxfTg8NI/dPY1oq','antdawgsmma@yahoo.com','Anthony','Figueroa',NULL,NULL,'active','125-310-9125','owner',1048,51,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$gXyETGI1.PNm71uqNp8QduguzhenRJkzHbIMwGqzB/n5cy6iKFT8e','haywardbjj@gmail.com','David','Thompson',NULL,NULL,'active','425-999-5425','manager',1014,52,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$3OHev7eOxBmiHgp0y5BsnuWaUZF2eoSF5EVYV2bDzXVTGk.0iInd6','jeanelle.pilates@gmail.com','Jeanelle','Singh',NULL,NULL,'active',NULL,'owner',1054,53,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$77d5Eyk/53BcWLdIHakAHOnpPlq.ehMn7ubqLzXjhIm4YcIBghtw.','ak6233@gmail.com','Alex','Khanbabian',NULL,NULL,'active','254-668-5254','owner',1056,54,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$ztc4/W59ELXTxvNh4sB6w.pR6JOWl.61RDZuwghjfGy12QDRuyPoS','modern.taekwondo@gmail.com','John','Bernardo',NULL,NULL,'active','972-442-3972','owner',1060,55,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$rpZRQkdtT7nMV1H5HbboFOhRM80wsjKBtn5Oymlsu/XYRl3lsqe4a','anchoredsc@gmail.com','Steve','Kpa',NULL,NULL,'active','663-708-4663','owner',1062,56,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$hNBelQXpjL1wVcRr8jttfeqmKaX.bImmV2.Zk1lkr7vWlc8AvChiS','debbie@socialfitnessnetwork.com','Debbie','Sanchez',NULL,NULL,'active','607-499-4607','admin',NULL,57,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$KPg8Z5iJE/jcNNUooy9E2OZ2l7IIx9/U21yjh1nD3P01hhgHwMtm.','mastersteve@hrksf.com','Master Steve','Rapport',NULL,NULL,'active','556-235-7556','owner',1064,58,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$.CVo3j7Ki01z7YsEfV4VU.dGFlERRuTOliymS5k.PSupH.NtRwo5W','angelhalolopez@gmail.com','Angel','Lopez',NULL,NULL,'active','183-213-0183','manager',1066,59,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$ka2D5keyqWHVHyq/2EuJA.xfIhjP/iMVQIJ7nD4FmgARyOLMXJyJu','dojo@claycombkarate.com','Joseph','Claycomb',NULL,NULL,'active','642-829-1642','owner',1068,60,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$KCOH74uYoyOvw0RW5OSvFeYt/mAeT5uBi0zW1OQpo3pO57lpJi3Bq','jose@smashmilpitas.com','Jose','Palacios',NULL,NULL,'active','431-688-5431','manager',1018,63,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$BoeQLQI6BA463rehHqJan.aWV4jKmtUx3cvg5cg4L7Wv0seG8ci1u','sanjose@smashgyms.com','Smash','SanJose',NULL,NULL,'active','881-239-0881','employee',1016,64,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$xn64aK7VyV./1VHQ08OkteNQJ3H11vLwB.Asp9jZkEGGTVcsdT8FC','thekaizendojo@gmail.com','William','Ford',NULL,NULL,'active','181-626-5181','owner',1074,65,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$UpbSsCv8JMDWs7rtWMQSoOAUodUG3wNSNE4B5k2O2KUQwPEKrPPfm','profactionmma@gmail.com','Geoff','Quares',NULL,NULL,'active','644-750-1644','owner',1078,66,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$KfulbJZQeFMSUycnlvbTtuqb/il3iM.MXTDYN9aiSm6G0i.DAcG4y','info@knoxxgym.com','Josh','Thomson',NULL,NULL,'active','073-809-7073','owner',1080,67,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$MWzjXusZf.kuq0PfcbOwoOk79G0li85RK5mk2B4DwUkvRaJ8U1HC2','jgrant.ber@gmail.com','John','Bernardo',NULL,NULL,'active',NULL,'manager',1014,68,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$SLGa5QuLwpP8V83OffqlaO0kGYkezhlpR/8OmmIq0U2j/con9L.ha','jgrant.ber+sfn.owner@gmail.com','John','Bernardo',NULL,NULL,'active',NULL,'owner',1014,69,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$tpXKQR2NBLLvV/RS0o4h5uQ9qT0KHvSRa0pwHSI0qDES3IY7WencK','jgrant.ber+sfn.employee@gmail.com','John','Bernardo',NULL,NULL,'inactive','111-111-1111','employee',1014,70,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$D/zSBGiQupiqOaIOWhITYOcbRmCQhuuuirfUzYD83uf4cdNFBqk0a','chris@unioncityfitnessandperformance.com','Christopher','Robertson',NULL,NULL,'active','234-935-4234','owner',1082,72,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$xDP3ZMDzOSJJJLf4S/kgSeKTIlrrNJrEoVRBTbRq4Ttey4fsm6ImK','alisa+test@profitablemedia.com','Pmedia','Test',NULL,NULL,'active','793-600-2793','manager',1014,73,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$R6NgaruZ4RKvNYEfZLe01u7rrHs1/oOQmUQwwv3BPbZzQnmnhv/m.','tntwc98@yahoo.com','George','Tsutsui',NULL,NULL,'active','898-934-9898','owner',1084,74,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$J2mHYtlH/yeHcEm3FqIInudSXj35ApZD4cBjwurde6w9nm2xsyTEC','webops+sfnadmin@profitablemedia.com','Pmedia','Admin',NULL,NULL,'active','793-600-2793','admin',NULL,75,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$f3XQstRhHdGnPKkaFlr.ueVySYWz54tTErBG1DMiuyRc7lBqPgNf.','webops+sfnowner@profitablemedia.com','Pmedia','Gym Owner',NULL,NULL,'active','793-600-2793','owner',1014,76,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$jvnaeMrJU5UiS5JICUzYwOA/OsgpnaSFafvRrg6lRv30OVDISjAa6','webops+sfnmanager@profitablemedia.com','Pmedia','Gym Manager',NULL,NULL,'active','793-600-2793','manager',1014,77,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$DEUqL94VuDjDu4OCKfwreOetrzmXKP12OMJjkNZBf1wNqUeUpFr22','webops+sfnemployee@profitablemedia.com','Pmedia','Gym Employee',NULL,NULL,'active','793-600-2793','employee',1014,78,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$Xf09eECaWO5BfWINiI6sYOLxooA0qgn6xkswB1H94dc6c387Pu20a','palacios901s@yahoo.com','Jose','palacios',NULL,NULL,'active','431-688-5431','owner',1040,79,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$a8txIcGR8XSzJA7eNN6aF.e3jgNAlpMjAgvP/uHlqostbxDxYORlS','jgrant.ber+sfn.admin@gmail.com','John','Bernardo',NULL,NULL,'active','416-759-5416','admin',NULL,80,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$P4ggd9FvoEzh2dgtDC5CSeChnKpm.Gj44vLnzZjmhmrSuPfNd.TKG','sgomez@true10fitness.com','Stephanie','Gomez',NULL,NULL,'active','706-241-6706','owner',1086,81,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$z1jpzODU4n0UyEXkwHr9Ve/JKjDRt0/FPq6Zu3QEel6K22jM6NLxa','jgrant.ber+sfn.manager@gmail.com','John','Bernardo',NULL,NULL,'active',NULL,'manager',1014,82,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$40ZfzDCCvO3nORmOg41YU.eDHHQpJfAwZHaPWA33weBtVpEyWlcZC','mike@tomacelliacademy.com','Mike','Tomacelli',NULL,NULL,'active','106-371-5106','owner',1088,83,'2017-02-02 16:47:12','2017-04-28 22:15:55'),('$2y$08$ymahwuS994/nJuQ08QJFIubx8A7gN9XxRxkqRB8c4hgbMKzI3twe2','christine@socialfitnessnetwork.com','Christine','Ho',NULL,NULL,'active',NULL,'admin',NULL,84,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$TQ1VooluWF./W.DFPMmyv.wZ/9.N/s5xbV7t64Iv1Cvf5IeYEXbuO','larry@westcoastathletics.net','Lawrence','Anair',NULL,NULL,'active','555-705-9555','owner',1092,86,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$ERagwewtG1t/3nzl2ENPoupWp2550DZhZUWKjR.Jrw.4QZl95tpDK','raulcastillomartialarts@gmail.com','Raul','Castillo',NULL,NULL,'active','100-560-0100','manager',1014,87,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2y$08$IryTBzo4dwP8Nfb2PfrPTOlpaK8JNz1w4CaNyouU7VU9l/hd1XRyi','nicoyamipoya13@gmail.com','Jimmy','Jarquin',NULL,NULL,'active','264-261-0264','owner',1090,88,'2017-02-02 16:47:12','2017-02-02 16:47:12'),('$2a$10$HQR22KPDgr1G/IpbNq08c.I8jOUDz6fRwSZVYdc9iOlbS4m2NE80K','cainan+testDH@socialfitnessnetwork.com','Ca','Mun','','{}','active','5862433767','owner',6,2001,'2017-02-20 23:02:32','2017-02-20 23:02:32'),('$2a$10$aBc2BqV.EFahUcm.M/J2r.1YM/r53Jnv8vR.fkGrlY6oQSRlMf7GC','jgrantber+owner1@gmail.com','John','Bernardo','','{}','active','9257595416','owner',-1,2002,'2017-02-20 23:04:08','2017-02-20 23:04:08'),('$2a$10$qzuMdEtsXdTEd60bG0FtO.u40wt18KhVUsC3z.7m3zFgkSSP3wQMy','jgrantber+employee1@gmail.com','John','Bernardo','','{}','active','9257595416','employee',-1,2003,'2017-02-20 23:05:57','2017-02-20 23:05:57'),('$2a$10$N.uh5/iJ0LLkKB7i1yVPFe091QwuFIO7LYZ0s2BfOLznzbgprRl.S','jg+ownerDH@gmail.com','John','Bernardo','','{}','active','9257595416','owner',6,2004,'2017-02-20 23:48:18','2017-02-20 23:48:18'),('$2a$10$DVnAouEDX4mfNFsQ8ekjcO2lhXU1VqAXq5Jkclntbgm4V1FpJOYwO','khoi@socialfitnessnetwork.com','Khoi','Nguyen','','{}','active','','superAdmin',NULL,2005,'2017-04-28 22:14:34','2017-04-28 22:14:34');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-03 20:00:12
